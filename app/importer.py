import re
import math

from app.Illuminate.ParamStorage import ParamStorage


def N_to_newN(N):
    N1 = N[:2]
    N2 = N[2:]
    N1 = float(N1)
    N2 = float(N2) / 60
    rez_N = N1 + N2
    return rez_N


def E_to_newE(E):
    E1 = E[1:3]
    E2 = E[3:]
    E1 = float(E1)
    E2 = float(E2) / 60
    rez_E = E1 + E2
    return rez_E


def speed(lat_old, long_old, time_old, lat_new, long_new, time_new):  # Определение скорости по координатам и времени
    time1 = re.findall(r"\d+", time_old)
    time2 = re.findall(r"\d+", time_new)
    time_res1 = int(time1[0]) * 3600 + int(time1[1]) * 60 + int(time1[2])
    time_res2 = int(time2[0]) * 3600 + int(time2[1]) * 60 + int(time2[2])
    time = time_res2 - time_res1  # Время между двумя точками ( в секундах)

    R = 6371  # Средний радиус Земли

    lat_old = lat_old * math.pi / 180
    lat_new = lat_new * math.pi / 180
    long_old = long_old * math.pi / 180
    long_new = long_new * math.pi / 180

    # Определение расстояния с помощью гаверсинуса (в метрах)
    distance = 2 * 1000 * R * math.asin(math.sqrt(
        math.sin((lat_new - lat_old) / 2) ** 2 + math.cos(lat_old) * math.cos(lat_new) * math.sin(
            (long_new - long_old) / 2) ** 2))

    if time != 0:
        return round(distance / time, 4)
    else:
        return 0


def process(storage: ParamStorage) -> list:
    result = []
    res = []

    filename = storage.get_file_by_type('log_file')
    date_time = storage.get_params()['date_time']

    row_numb = 1

    ex_time = 0
    ex_N = 0
    ex_E = 0
    ex_K = 0
    flag_time = '00:00:00'  # Флаг для того, чтобы убрать дублирующие WIMDA по одному GPGGA

    old_N = 0
    old_E = 0

    with open(filename, encoding='UTF-8') as log_file:
        for line in log_file.readlines():
            if re.match(r".*GPGGA", line) is not None:
                # time, line = re.split(r"\.\d+:\s+<-", line)
                empty_flag = False  # Флаг против нулевых значений GPGGA
                time = re.split(r"\.", line)
                N = re.findall(r"(\d+\.\d+)(\,N)", line)  # Широта

                E = re.findall(r"(\d+\.\d+)(\,E)", line)  # Долгота

                if N and E:
                    ex_time = time[0]
                    ex_N = N_to_newN(N[0][0])
                    ex_E = E_to_newE(E[0][0])
                else:
                    empty_flag = True

            elif re.match(r".*GPVTG", line) is not None:
                K = re.findall(r"(\d+\.\d)(\,K)", line)  # Скорость судна
                if K:
                    new_K = float(K[0][0]) * 1000 / 3600  # Перевод в м/c
                    ex_K = round(new_K, 4)

            elif re.match(r".*WIMDA", line) is not None:
                B = re.findall(r"(\d+\.\d+)(\,B)", line)  # Атмосферное давление
                C = re.findall(r"(\d+\.\d)(\,C)", line)  # Температура воздуха
                T = re.findall(r"(\d+\.\d)(\,T)", line)  # Направление ветра
                M = re.findall(r"(\d\.\d)(\,M\*)", line)  # Скорость ветра

                if ex_time != 0 and ex_N != 0 and ex_E != 0 and flag_time != ex_time and not (
                empty_flag) and B and C and T and M:
                    ex_time = str(ex_time).split('.')[1] if "." in str(ex_time) else ex_time

                    res = dict({
                        'date': date_time,
                        'step_id': row_numb,
                        'time': ex_time,
                        'latitude': round(ex_N, 4),
                        'longitude': round(ex_E, 4),
                        'speed': ex_K,
                        'barometric_pressure': B[0][0],
                        'air_temperature': C[0][0],
                        'wind_direction': T[0][0],
                        'wind_speed': M[0][0],
                        'vehicle_speed': speed(old_N, old_E, flag_time, ex_N, ex_E, ex_time)
                    })

                    result.append(res)
                    row_numb += 1

                    ex_K = 0
                    flag_time = ex_time
                    old_N = ex_N
                    old_E = ex_E

    return result
